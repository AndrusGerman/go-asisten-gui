package gui

import (
	"github.com/zserge/lorca"
)

// NewLorcaHTTP launc windows lorca http
func NewLorcaHTTP(route string, width int, height int) (lorca.UI, error) {
	lorca.ChromeExecutable = LocateChrome
	return lorca.New(route, "", width, height)
}
