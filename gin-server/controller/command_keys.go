package controller

import (
	"log"
	"net/http"
	"runtime"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/models"
)

func CommandKeysAllHTML(ctx *gin.Context) {
	var commandKeys []models.CommandKey
	Db.Find(&commandKeys)
	var os []models.OS
	Db.Table("os").Find(&os)
	for i, j := 0, len(commandKeys)-1; i < j; i, j = i+1, j-1 {
		commandKeys[i], commandKeys[j] = commandKeys[j], commandKeys[i]
	}
	ctx.HTML(200, "command_keys_all.html", gin.H{
		"CommandKeys": commandKeys,
		"OS":          os,
	})
}

func CommandKeysAddAPI(ctx *gin.Context) {
	var commandKey = new(struct {
		models.CommandKey
		OS string
	})
	ctx.BindQuery(commandKey)
	if commandKey.OS == "" {
		log.Println("ERROR: OS is null, used default")
		commandKey.OS = runtime.GOOS
	}
	if commandKey.Key == "" || commandKey.Command == "" {
		log.Println("ERROR: Faltan parametros en commandKey")
		ctx.Redirect(http.StatusTemporaryRedirect, "/config/command_keys")
		return
	}
	var getOS = new(models.OS)
	Db.Table("os").Find(getOS, commandKey.OS)
	commandKey.Key = strings.ToUpper(commandKey.Key)
	commandKey.OsID = getOS.ID
	Db.Create(&commandKey.CommandKey)
	ctx.Redirect(http.StatusTemporaryRedirect, "/config/command_keys")
}

func CommandKeysDeleteAPI(ctx *gin.Context) {
	id := ctx.Param("id")
	Db.Delete(&models.CommandKey{}, id)
	ctx.Redirect(http.StatusTemporaryRedirect, "/config/command_keys")
}
