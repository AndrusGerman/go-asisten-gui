package controller

import (
	"log"
	"net/http"
	"runtime"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/models"
)

func ProgramsAllHTML(ctx *gin.Context) {
	var programs []models.ProgramsAlternative
	var os []models.OS
	Db.Table("os").Find(&os)
	Db.Find(&programs)
	for i, j := 0, len(programs)-1; i < j; i, j = i+1, j-1 {
		programs[i], programs[j] = programs[j], programs[i]
	}
	ctx.HTML(200, "programs_all.html", gin.H{
		"Elements": programs,
		"OS":       os,
	})
}

func ProgramsDeleteAPI(ctx *gin.Context) {
	id := ctx.Param("id")
	Db.Delete(&models.ProgramsAlternative{}, id)
	ctx.Redirect(http.StatusTemporaryRedirect, "/config/programs")
}

// ProgramsAddAPI Default
func ProgramsAddAPI(ctx *gin.Context) {
	programsString, _ := ctx.GetQuery("Programas")
	programs := strings.Split(programsString, ",")
	generalName, _ := ctx.GetQuery("GeneralName")
	os, _ := ctx.GetQuery("OS")
	if len(programs) == 0 {
		log.Println("ERROR: Not Programs")
		ctx.Redirect(http.StatusTemporaryRedirect, "/config/programs")
		return
	}
	if generalName == "" {
		log.Println("ERROR: Not GeneralName")
		ctx.Redirect(http.StatusTemporaryRedirect, "/config/programs")
		return
	}
	if os == "" {
		log.Println("ERROR: OS is null, used default")
		os = runtime.GOOS
	}
	var getOS = new(models.OS)
	Db.Table("os").Find(getOS, os)
	var pro = new(models.ProgramsAlternative)
	pro.OsID = getOS.ID
	pro.Programs = strings.Join(programs, "|")
	pro.GeneralName = strings.ToUpper(generalName)
	Db.Create(pro)
	ctx.Redirect(http.StatusTemporaryRedirect, "/config/programs")
}
