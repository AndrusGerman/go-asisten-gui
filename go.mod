module gitlab.com/AndrusGerman/go-asisten-gui

go 1.15

replace gitlab.com/AndrusGerman/go-asisten-core => ../go-asisten-core

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/otiai10/copy v1.4.2 // indirect
	github.com/zserge/lorca v0.1.9
	gitlab.com/AndrusGerman/go-asisten-core v0.0.0-20210124060138-3bb8141323b5
)
