package main

import (
	"flag"
	"log"
	"os"

	extra "gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/extras"
)

// ServerPort config
var ServerPort = ""

// GoAsistenPreDataDir dir
var GoAsistenPreDataDir = ""

// GoAsistenDataDir dir
var GoAsistenDataDir = ""

// GoAsistenGUIDir dir
var GoAsistenGUIDir = ""

// GoAsistenResetDataDir bool
var GoAsistenResetDataDir bool

func init() {
	flag.StringVar(&ServerPort, "server-port", "1325", "Server port")
	flag.StringVar(&GoAsistenPreDataDir, "go-predata-dir", "./../go-asisten-core/data/", "Pre Data Dir")
	flag.StringVar(&GoAsistenDataDir, "go-data-dir", "./data/", "Data Dir")
	flag.StringVar(&GoAsistenGUIDir, "go-gui-dir", "./", "Gui Dir")
	flag.BoolVar(&GoAsistenResetDataDir, "go-reset-config", false, "Reset Config")
	flag.Parse()
}

func installPredata() {
	log.Println("go-asisten-gui: Instalando configuración")
	// Find Pre Data file
	if _, err := os.Stat(GoAsistenPreDataDir + "config/asisten.json"); os.IsNotExist(err) {
		log.Println("go-asisten-gui: Error install pre data ", err)
		os.Exit(1)
	}
	// Install Predata file
	err := extra.DataPrepare(GoAsistenDataDir, GoAsistenPreDataDir)
	if err != nil {
		log.Println("go-asisten-gui: Error move pre data ", err)
		os.Exit(1)
	}

}
