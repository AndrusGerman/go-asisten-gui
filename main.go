package main

import (
	"log"
	"os"

	"github.com/zserge/lorca"
	newCore "gitlab.com/AndrusGerman/go-asisten-core"
	extra "gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/extras"
	server "gitlab.com/AndrusGerman/go-asisten-gui/gin-server"
	gui "gitlab.com/AndrusGerman/go-asisten-gui/gui-lorca"
)

func main() {
	// Parse dirs
	GoAsistenDataDir = extra.ParseDirsEnv(GoAsistenDataDir)
	GoAsistenPreDataDir = extra.ParseDirsEnv(GoAsistenPreDataDir)

	// Install config data
	if _, err := os.Stat(GoAsistenDataDir + "config/asisten.json"); os.IsNotExist(err) {
		installPredata()
	}

	// Create new Core Client
	coreClient, err := newCore.NewGoAsistenCore(GoAsistenDataDir)
	if err != nil {
		log.Println("Error in open core: ", err)
		os.Exit(1)
	}

	// Create Server Http For View
	server.InitServer(ServerPort, coreClient.Db, GoAsistenGUIDir)

	// Open gui HTTP
	ui, err := gui.NewLorcaHTTP("http://localhost:"+ServerPort+"/asisten", 360, 360)
	if err != nil {
		log.Println("Error open chrome gui: ", err)
		os.Exit(1)
	}

	// Closed App Methods
	closeApp := func() {
		coreClient.Db.Close()
		ui.Close()
		os.Exit(0)
	}
	defer closeApp()

	// Set Methods Speech
	ui.Bind("speechAndText", speechProcess(coreClient))

	// Get Lang
	ui.Bind("getLang", func() string { return coreClient.Clientts.Lang })

	// Set Logs Speech
	coreClient.SetSystemLog(speechText(coreClient, ui))

	// Closed UI on GUI
	ui.Bind("StopGUI", func() {
		closeApp()
	})

	// Waiting finish app
	<-ui.Done()
}

func speechProcess(coreClient *newCore.AsistenCoreModel) func(string) []string {
	return func(words string) []string {
		// client Words convert in Asisten response
		responseAsisten, funcAsisten := coreClient.WordsToAsistenResponse(words)

		// Asisten Function run
		funcAsisten()

		// Convert AsisteWords in Speech Base64 for JavaScript
		bs64Speech := coreClient.WordsToSpeechBase64(responseAsisten)

		// Return Response
		return []string{responseAsisten, bs64Speech}
	}
}

func speechText(coreClient *newCore.AsistenCoreModel, ui lorca.UI) func(string) {
	return func(words string) {
		// Convert words logs to bs64 speech
		bs64Speech := coreClient.WordsToSpeechBase64(words)
		ui.Eval(`speech("` + bs64Speech + `")`)
	}
}
